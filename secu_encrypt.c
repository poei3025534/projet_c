#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void setXy(int *x, int *y, char *key){
  int len = strlen(key);
  if(len % 2 == 1)
    len--;
  for(int i = 0; i < len/2; i++){
    *x += key[i] - 96; 
  } 
  for(int i = len/2; i < len; i++){
    *y += key[i] - 96; 
  } 
}

void copyFile(FILE *in, FILE *out){
  int byte;
  while((byte = fgetc(in)) != EOF)
    fputc(byte, out);
  fseek(in, 0, SEEK_SET);
  fseek(out, 0, SEEK_SET);
}

void code(FILE *out, FILE *in, int x, int y, int len, char *mod){
  int tmpX, tmpY;
  int curX = x, curY = y;
  if(strcmp(mod, "-d") == 0){
    curX = x * len;
    curY = y * len;
  }
  for(int i = 0; i < len; i++){
    fseek(out, curX, SEEK_SET);
    tmpX = fgetc(out);
    fseek(out, curY, SEEK_SET);
    tmpY = fgetc(out);
    fseek(out, curX, SEEK_SET);
    fputc(tmpY, out);
    fseek(out, curY, SEEK_SET);
    fputc(tmpX, out);
   
    printf("%x %x\n", tmpY, tmpX); 
    if(strcmp(mod, "-d") == 0){
      curX -= x;
      curY -= y;
    } else {
      curX += x;
      curY += y;
    }
  }
}

int main(int argc, char *argv[])
{
  char *key = argv[3];
  char *file_name = argv[5];
  char *output_name = file_name;
  int x = 0, y = 0, pivot = 0;
  int len = strlen(key);

  if(len < 4 || len > 10){
    fprintf(stderr, "La clé doit être de longeur comprise entre 4 et 10.\n");
    return EXIT_FAILURE;
  }
  for(int i = 0; i < len; i++){
    if(key[i] > 'z' || key[i] < 'a'){
      fprintf(stderr, "La clé doit contenir uniquement des caractères de l'alphabet minuscule.\n");
      return EXIT_FAILURE;
    }
  }

  FILE *fptr, *fpout;
  fptr = fopen(file_name, "rb");

  fseek(fptr, 0 , SEEK_END);
  int size = ftell(fptr);
  fseek(fptr, 0 , SEEK_SET);

  if(size < 1000 || size > 100000){
    fprintf(stderr, "Le fichier doit être compris entre 1KB et 100KB\n");
    return EXIT_FAILURE;
  }

  if(strcmp(argv[1], "-e") == 0)
    strcat(output_name, ".enc");
  else if(strcmp(argv[1], "-d") == 0)
    output_name[strlen(output_name) - 4] = '\0';
  else{
    fprintf(stderr, "La premiere option doit être -e ou -d.\n");
    return EXIT_FAILURE;
  }
  
  fpout = fopen(output_name, "w+b"); 

  setXy(&x, &y,key);
  copyFile(fptr, fpout);
  code(fpout, fptr, x, y, len, argv[1]);

  fclose(fptr);
  fclose(fpout);

  return EXIT_SUCCESS;
}
