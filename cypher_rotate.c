#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char * cypher_rotate(char * string, int sens, int shift){
  int len = strlen(string);
  for(int i = 0; i < len ; i++){
    if(string[i] >= 97 && string[i] <= 122){
      if(sens == 0){
        if(string[i] + shift > 122)
          string [i] = 96 + ((string[i] + shift) - 122);
        else 
          string [i] = string[i] + shift;
      } else {
        if(string[i] - shift < 97)
          string[i] = 122 - (96 - (string[i] - shift));
        else
          string[i] = string[i] - shift;
      }
    }
  }

  return string;
}

int main(int argc, char *argv[])
{
  printf("%s\n", cypher_rotate(argv[1],atoi(argv[2]),atoi(argv[3])));
  return EXIT_SUCCESS;
}
