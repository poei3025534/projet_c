#include <stdio.h>
#include <stdlib.h>

typedef char BYTE;

int isPng(FILE *fptr){
  BYTE key[] = { 0x89, 0x50, 0x4E, 0x47, 0x0D, 0x0A, 0x1A, 0x0A };
  int keyLen = 8;
  BYTE curByte;
  for (int i = 0; i < keyLen; i++){
    curByte = fgetc(fptr);
    if(key[i] != curByte){
      fseek(fptr, - (i + 1), SEEK_CUR);
      return 0;
    }
  }
  fseek(fptr, - keyLen , SEEK_CUR);
  return 1;
}

int isJpg(FILE *fptr){
  BYTE key[] = { 0xFF, 0xD8, 0xFF, 0xE0 };
  int keyLen = 4;
  BYTE curByte;
  for (int i = 0; i < keyLen; i++){
    curByte = fgetc(fptr);
    if(key[i] != curByte){
      fseek(fptr, - (i + 1), SEEK_CUR);
      return 0;
    }
  }
  fseek(fptr, - keyLen , SEEK_CUR);
  return 1;
}

int isExe(FILE *fptr){
  BYTE key[] = { 0x4D, 0x5A };
  int keyLen = 2;
  BYTE curByte;
  for (int i = 0; i < keyLen; i++){
    curByte = fgetc(fptr);
    if(key[i] != curByte){
      fseek(fptr, - (i + 1), SEEK_CUR);
      return 0;
    }
  }
  fseek(fptr, - keyLen , SEEK_CUR);
  return 1;
}

int isPdf(FILE *fptr){
  BYTE key[] = { 0x25, 0x50, 0x44, 0x46, 0x2D };
  int keyLen = 5;
  BYTE curByte;
  for (int i = 0; i < keyLen; i++){
    curByte = fgetc(fptr);
    if(key[i] != curByte){
      fseek(fptr, - (i + 1), SEEK_CUR);
      return 0;
    }
  }
  fseek(fptr, - keyLen , SEEK_CUR);
  return 1;
}

int isDoc(FILE *fptr){
  BYTE key[] = { 0xD0, 0xCF, 0x11, 0XE0, 0xA1, 0xB1, 0x1A, 0xE1 };
  int keyLen = 8;
  BYTE curByte;
  for (int i = 0; i < keyLen; i++){
    curByte = fgetc(fptr);
    if(key[i] != curByte){
      fseek(fptr, - (i + 1), SEEK_CUR);
      return 0;
    }
  }
  fseek(fptr, - keyLen , SEEK_CUR);
  return 1;
}

char * getHeaderName(FILE *fptr){
  if(isJpg(fptr))
    return "jpg";
  if(isPng(fptr))
    return "png";
  if(isExe(fptr))
    return "exe";
  if(isPdf(fptr))
    return "pdf";
  if(isDoc(fptr))
    return "doc";

  return "Unknow file type";
}

int main(int argc, char *argv[])
{
  FILE *fptr;
  fptr = fopen(argv[1], "rb");

  printf("Type du fichier : %s\n", getHeaderName(fptr));

  // printf("%d\n", isJpg(fptr));
  // printf("%d\n", isPng(fptr));
  // printf("%d\n", isExe(fptr));
  // printf("%d\n", isPdf(fptr));
  // printf("%d\n", isDoc(fptr));

  return EXIT_SUCCESS;
}
