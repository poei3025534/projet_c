#include <stdio.h>
#include <stdlib.h>
#include <time.h>

char * generate(int len, int spec, char *password){
  int nbType;
  if (spec)
    nbType = 4;
  else
    nbType = 3;
  
  int choice;
  char curChar;
  srand(time(NULL));
  for(int i = 0; i<len; i++){
    choice = rand() % nbType;
    switch (choice) {
      case 0:
        curChar = rand() % 26 + 97;
        password[i] = curChar;
        break;
      case 1:
        curChar = rand() % 26 + 65;
        password[i] = curChar;
        break;
      case 2:
        curChar = rand() % 10 + 48;
        password[i] = curChar;
        break;
      case 3:
        curChar = rand() % 13 + 33;
        password[i] = curChar;
        break;
      default:
        break;
    }
  }
  return password;
}

int main(int argc, char *argv[])
{
  int passwordLen = atoi(argv[1]);
  int withSpecial = atoi(argv[2]);

  char password[passwordLen];
  printf("%s\n", generate(passwordLen, withSpecial, password));
  return EXIT_SUCCESS;
}
