#include <stdio.h>
#include <stdlib.h>
#include <string.h>


int getNbLine(FILE *fptr){
  int nbLine = 0;
  int byte;
  while((byte = fgetc(fptr)) != EOF){
    if( byte == '\n')
      nbLine++;
  }
  fseek(fptr, 0, SEEK_SET);
  return nbLine;
}

int getNbChar(FILE *fptr){
  int nbChar = 0;
  int byte;
  while((byte = fgetc(fptr)) != EOF){
    if( byte >= 33 && byte <= 126)
      nbChar++;
  }
  fseek(fptr, 0, SEEK_SET);

  return nbChar; 
}

int getNbWord(FILE *fptr){
  int nbWord = 0;
  int byte;
  int isFirst = 1;
  while((byte = fgetc(fptr)) != EOF){
    if(isFirst == 1){
      if(byte >= 33 && byte <= 126){
        nbWord = 1;
      } 
      isFirst = 0;
    }
    if((byte == ' ' || byte == '\n' || byte == '\t') && byte != EOF){
      do{
        byte = fgetc(fptr);
      } while ( (byte < 33 || byte > 126) && byte != EOF);
      nbWord++;
    }
  }
  fseek(fptr, 0, SEEK_SET);
  nbWord--;

  return nbWord;
}

int main(int argc, char *argv[])
{
  int idName;  
  FILE *fptr;

  if(strcmp(argv[1], "-c") == 0 && argc != 3){
    fprintf(stderr, "Nom de fichier manquant.\n");
    return EXIT_FAILURE;
  }
  if(argc == 3){
    idName = 2;
  } else if (argc == 2){
    idName = 1;
  } else {
    fprintf(stderr, "Commande non comforme.\n");
    return EXIT_FAILURE;
  }

  fptr = fopen(argv[idName], "rb");

  if(strcmp(argv[1], "-c") == 0)
    printf("%d %s\n", getNbChar(fptr), argv[idName]);
  else
    printf("%d %d %d %s\n", getNbLine(fptr), getNbWord(fptr), getNbChar(fptr), argv[idName]);

  fclose(fptr);

  return EXIT_SUCCESS;
}
