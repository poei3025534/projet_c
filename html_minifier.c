#include <stdio.h>
#include <stdlib.h>


void minifier(FILE *fptrI, FILE *fptrO){
  int curChar;
  while((curChar = fgetc(fptrI)) != EOF){
    if(curChar != 0x0A){
      fputc(curChar, fptrO);
    }
    else {
      curChar = fgetc(fptrI);
      while(curChar == 0x20 || curChar == 0x0A && curChar != EOF)
        curChar = fgetc(fptrI);
      
      fputc(curChar, fptrO);
    }
  }
}

int main(int argc, char *argv[])
{
  FILE *fptrI, *fptrO;
  fptrI = fopen(argv[1], "r+");
  fptrO = fopen(argv[2], "w+");

  minifier(fptrI, fptrO);

  return EXIT_SUCCESS;
}
