#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int isLongEnough(char * pass){
  if (strlen(pass) >= 10)
    return 1;
  else
    return 0;
}

int haveMaj(char * pass){
  int len = strlen(pass);
  for(int i = 0; i<len; i++){
    if(pass[i] >= 65 && pass[i] <= 90)
      return 1;
  }
  return 0;
}

int haveNum(char * pass){
  int len = strlen(pass);
  for(int i = 0; i<len; i++){
    if(pass[i] >= 48 && pass[i] <= 57)
      return 1;
  }
  return 0;
}

int haveSpec(char * pass){
  int len = strlen(pass);
  for(int i = 0; i<len; i++){
    if(pass[i] >= 33 && pass[i] <= 47)
      return 1;
  }
  return 0;
}

int isPasswordStrong(char * pass){
  if(isLongEnough(pass) && haveMaj(pass) && haveNum(pass) && haveSpec(pass))
    return 1;
  else
    return 0;
}

int main(int argc, char *argv[])
{

  int isStrong = isPasswordStrong(argv[1]);
  printf("%d\n", isStrong);
  return EXIT_SUCCESS;
}
